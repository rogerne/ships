class GamesController < ApplicationController
  before_action :signed_in_user, only: [:new, :show, :edit, :update, :destroy]
  before_action :set_game, only: [:show, :edit, :update, :destroy]
  before_action :correct_user,   only:  [:edit, :update, :destroy]

  # GET /games
  # GET /games.json
  def index
    @games = Game.all
  end

  # GET /games/1
  # GET /games/1.json
  def show
  end

  # GET /games/new
  def new
    puts "current_user = #{current_user.id}"

    @game = Game.new()
  end

  # GET /games/1/edit
  def edit
  end

  # POST /games
  # POST /games.json
  def create
    @game = current_user.games.new(game_params)
    #@game = Game.new(game_params)
    if @game.save
      redirect_to edit_game_path(@game) 
    else
      render :new
    end
  end

  # PATCH/PUT /games/1
  # PATCH/PUT /games/1.json
  def update
    respond_to do |format|
      #puts "In update"
      if @game.update(game_params)
        format.html { redirect_to edit_game_path(@game), notice: 'Game was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /games/1
  # DELETE /games/1.json
  def destroy
    @game.destroy
    respond_to do |format|
      format.html { redirect_to games_url, notice: 'Game was successfully destroyed.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_game
      @game = Game.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def game_params
      params.require(:game).permit(:pA_name, :pB_name, :visible_grid, :direction, :x_pos, :y_pos, :auto_deploy, :user_id)
      #params.permit(:p1_name, :p2_name, :visible_grid, :direction, :x_pos, :y_pos)
    end

    def correct_user
      @game = current_user.games.find_by_id(params[:id])
      if @game.nil?
        flash.now[:danger] = 'Only the owner can play this game.'
        redirect_to games_path 
      end
    end 
end