class GridDemo
  attr_accessor :grid, :grid_container
    
  def initialize
    @grid_container = Hash.new

    @grid = [ ["X","X","X","X","X","X","X","X","X","X"],
              ["X","X","X","X","X","X","X","X","X","X"],
              ["X","C","X","X","B","B","B","B","X","X"],
              ["X","C","X","X","X","X","X","X","X","X"],
              ["X","C","X","X","X","X","S","S","S","X"],
              ["X","X","X","X","X","X","X","X","X","X"],
              ["X","X","X","A","A","A","A","A","X","X"],
              ["X","X","X","X","X","X","X","X","X","X"],
              ["X","X","X","X","X","X","X","D","D","X"],                                                         
              ["X","X","X","X","X","X","X","X","X","X"] ]
    fill_grid
  end
    
private
  def fill_grid
  xVals = ["One","Two","Three","Four","Five","Six","Seven","Eight","Nine","Ten"]
  i = 0
  j = 0
       ("A".."J").each do |x|
          xVals.each do |y|
             #puts x+"-"+y
              puts "@grid #{i} #{j} = #{@grid[i][j]}"
	         @grid_container[x+"-"+y] = @grid[i][j]
             j += 1	
          end
        j = 0
        i += 1
       end
  end
end