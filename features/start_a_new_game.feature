Feature: Create a new game
  As a user
  In order to play a new game
  I want to create a game

 Scenario: creating a new game
 	Given I am on the home page
 	When I click on the new link
 	Then I should be taken to the new game page to enter the player names

