Given("I have the auto deploy checkbox selected") do
  visit root_path
  click_on 'Grid test'
  sleep(1)
  click_on 'New Game'
  fill_in('Pa name', :with => 'John')
  fill_in('Pb name', :with => 'Doe')
  sleep(1)
  click_on 'Create Game'
  sleep(1)
  page.check('game_auto_deploy') 
end

When("I click on Deploy Vessel") do
  click_on 'Deploy Vessel'
  sleep(2)
end

#Then("I should see Your Fleet is deployed. Press Play to continue.") do |string|
Then("I should see {string}") do |string|
  expect(page).to have_content string
end