Feature: Game plays itself
  As the tester
  In order to test the game
  I want it to play itself so I don't have to


Scenario: Playing a whole game from scratch
  Given I the computer plays a whole game against itself
  When The game ends
  Then I should see that there is a Winner