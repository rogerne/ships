Rails.application.routes.draw do
  scope "(:locale)", locale: /#{I18n.available_locales.join("|")}/ do
    resources :games
    root 'static_pages#home'
    get 'static_pages/home'
    get 'static_pages/help'
    get 'static_pages/about'
    get 'static_pages/contact'
    get 'static_pages/grid'
    get 'sessions/new'
    get 'users/new'
    #root  'welcome#index'
    #get 'welcome/index'
    get  '/signup',  to: 'users#new'
    
    resources :articles do
      resources :comments
    end

    resources :users

    get    '/login',   to: 'sessions#new'
    post   '/login',   to: 'sessions#create'
    delete '/logout',  to: 'sessions#destroy'
  end
end
