class AddAttributesToGames < ActiveRecord::Migration[5.2]
  def change
    add_column :games, :p1_hashed_display_grid, :string
    add_column :games, :p1_hashed_ocean_grid, :string
    add_column :games, :p1_hashed_game_grid, :string
    add_column :games, :p1_display_grid, :string
    add_column :games, :p1_ocean_grid, :string
    add_column :games, :p1_game_grid, :string
    add_column :games, :p2_hashed_display_grid, :string
    add_column :games, :p2_hashed_ocean_grid, :string
    add_column :games, :p2_hashed_game_grid, :string
    add_column :games, :p2_display_grid, :string
    add_column :games, :p2_game_grid, :string
    add_column :games, :p2_ocean_grid, :string
    add_column :games, :direction, :text
    add_column :games, :x_pos, :text
    add_column :games, :y_pos, :text
    add_column :games, :deploy_count, :text
    add_column :games, :state, :text
    add_column :games, :current_player, :text
    add_column :games, :p1_game_grid_vessels, :text
    add_column :games, :p2_game_grid_vessels, :text
    add_column :games, :sub_status, :text
    add_column :games, :p1_dg_history, :text
    add_column :games, :p2_dg_history, :text
    add_column :games, :message, :text
    add_column :games, :score, :text
    add_column :games, :can_save, :boolean
  end
end
