require 'rails_helper'

RSpec.describe DisplayGrid, type: :model do

	#before(:context) do
  before(:each) do
		@g = DisplayGrid.new 10
	end	

	context "When the computer has 1 hit" do

		it "it should generate 1 of 4 auto references" do
		  #p2 = DisplayGrid.new 10
		  @g.update_grid("C4", "H", "AC")
		  expect(@g.get_play).to eq("B4").or eq("D4").or eq("C3").or eq("C5")
		end
	end

	context "When the computer has 2 vertical hits" do

		it "it should generate one of two auto references" do
			#p2 = DisplayGrid.new 10
		  @g.update_grid("C4", "H", "AC")
		  @g.update_grid("B4", "H", "AC")
		  expect(@g.get_play).to eq("A4").or eq("D4")
		end
	end

	context "When the computer has 2 horizontal hits" do

		it "it should generate one of two auto references" do
			#p2 = DisplayGrid.new 10
		  @g.update_grid("C5", "H", "AC")
		  @g.update_grid("C4", "H", "AC")
		  expect(@g.get_play).to eq("C3").or eq("C6")
		end
	end

	context "When the game has adjacent vessels" do

		it "should track hits on each vessel individually" do
		  @g.update_grid("C4", "H", "AC")
		  @g.update_grid("D4", "H", "DE")
		  @g.update_grid("C5", "H", "AC")
		  expect(@g.get_play).to eq("C3").or eq("C6")
		end
	end

	context "When the computer sinks a vessel" do

		it "the history of hits should be empty" do
			#p2 = DisplayGrid.new 10
		  @g.update_grid("C5", "H", "CR")
		  @g.update_grid("C4", "H", "CR")
		  expect(@g.history["CR"].size).to eq 2
		  @g.update_grid("C6", "S", "CR")
		  expect(@g.history["CR"].size).to eq 0
		end
	end
end